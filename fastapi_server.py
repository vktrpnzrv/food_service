from fastapi import FastAPI, File, HTTPException, Body
from fastapi.middleware.cors import CORSMiddleware
from PIL import Image
from io import BytesIO
import json
import numpy as np
import logging
import requests
from keras_preprocessing import image
import base64


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def default():
    return


@app.get("/ping/")
async def ping():
    """
    ping method for api call
    Returns:
        200
    """
    return


def image_classifier(img: np.array):
    payload = {"instances": [{"input_image": img.tolist()}]}

    # Making POST request
    r = requests.post(
        "http://localhost:9000/v1/models/ImageClassifier:predict", json=payload
    )

    # Decoding results from TensorFlow Serving server
    pred = json.loads(r.content.decode("utf-8"))
    categories = [
        "healthy",
        "junk",
        "dessert",
        "appetizer",
        "mains",
        "soups",
        "carbs",
        "protein",
        "fats",
        "meat",
    ]
    results = np.array(pred["predictions"])[0]

    preds = {}

    # if len(results) > 0:
    for i in range(len(categories)):
        preds[categories[i]] = int(results[i] * 100 // 1)
    return preds


@app.post("/analyse/image")
async def food_image_analysis(file: bytes = File(...)):
    """
    Handles image file path and urls

    Args:
        file: file in binary format

    Returns:
        json string of classes and embedding lists, i.e: {"cls": [str(cls)], "embedding": [embedding.tolist()]}
    """
    try:
        img = Image.open(BytesIO(file)).resize((224, 224)).convert("RGB")

        np_img = image.img_to_array(img) / 255.0

        img = np_img.astype("float16")

        preds = image_classifier(img)
        return {"result": preds}

    except Exception as e:
        return {"result": "error ocused: " + str(e)}


@app.post("/analyse/string")
async def food_string_analysis(data: str = Body(...)):
    """
    Handles base64 encoded images

    Args:
        data: image encoded in base64 format

    Returns:
        json string of classes and embedding lists, i.e: {"cls": [str(cls)], "embedding": [embedding.tolist()]}
    """
    try:
        data = data.replace("data:image/jpeg;base64,","")

        data = bytes(data, 'utf-8')
        
        img = image.img_to_array(
            Image.open(
                BytesIO(base64.b64decode(data)),  # request.form['b64']
            ).resize((224, 224)).convert("RGB")
        ) / 255.

        img = img.astype("float16")
        
        preds = image_classifier(img)
        
        return {"result": preds}

    except Exception as e:
        return {"result": "error ocused: " + str(e)}


@app.get("/inference/static")
def inference_static(image_name: str = "sample_1.jpeg"):
    """
    get binary images method for api call
    Returns:

    """
    categories = [
        "healthy",
        "junk",
        "dessert",
        "appetizer",
        "mains",
        "soups",
        "carbs",
        "protein",
        "fats",
        "meat",
    ]
    try:
        img = (
            Image.open(f"_static/{image_name}", mode="r")
            .resize((224, 224))
            .convert("RGB")
        )
    except:
        raise HTTPException(
            500, detail={"status": f"{image_name} not found in static files on server"}
        )
    try:
        img = (
            Image.open(f"_static/{image_name}", mode="r")
            .resize((224, 224))
            .convert("RGB")
        )
        np_img = image.img_to_array(img) / 255.0
        img = np_img.astype("float16")
        payload = {"instances": [{"input_image": img.tolist()}]}
        r = requests.post(
            "http://localhost:9000/v1/models/ImageClassifier:predict", json=payload
        )
        pred = json.loads(r.content.decode("utf-8"))
        results = np.array(pred["predictions"])[0]
        if len(results) > 0:
            preds = {}
            for i in range(len(categories)):
                preds[categories[i]] = int(results[i] * 100 // 1)
            return {"result": preds}
    except:
        raise HTTPException(500, detail={"status": f"Analysis failed on {image_name}"})
    else:
        raise HTTPException(
            422, detail={"status": f"{image_name} returned empty results"}
        )
